﻿
using ShenNius.Auth.API.Domain.Entity.Common;
using SqlSugar;

namespace ShenNius.Mall.API.Domain.Entity
{
    ///<summary>
    ///订单商品表（不用商品id关联是因为商品会存在更新问题）
    ///</summary>
    [SugarTable("shop_order_goods")]
    public partial class OrderGoods : BaseTenantEntity
    {
        /// <summary>
        /// Desc:商品id
        /// Default:0
        /// Nullable:False
        /// </summary>           
        public int GoodsId { get; private set; }

        /// <summary>
        /// Desc:商品名称
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string GoodsName { get; private set; }

        /// <summary>
        /// Desc:商品封面图id
        /// Default:0
        /// Nullable:False
        /// </summary>           
        public string ImgUrl { get; private set; }

        /// <summary>
        /// Desc:库存计算方式(10下单减库存 20付款减库存)
        /// Default:20
        /// Nullable:False
        /// </summary>           
        public int DeductStockType { get; private set; }

        /// <summary>
        /// Desc:规格类型(10单规格 20多规格)
        /// Default:0
        /// Nullable:False
        /// </summary>           
        public int SpecType { get; private set; }

        /// <summary>
        /// Desc:商品sku标识
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string SpecSkuId { get; private set; }

        /// <summary>
        /// Desc:商品规格id
        /// Default:0
        /// Nullable:False
        /// </summary>           
        public int GoodsSpecId { get; private set; }

        /// <summary>
        /// Desc:商品规格信息
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string GoodsAttr { get; private set; }

        /// <summary>
        /// Desc:商品详情
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string Content { get; private set; }

        /// <summary>
        /// Desc:商品编码
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string GoodsNo { get; private set; }

        /// <summary>
        /// Desc:商品价格
        /// Default:0.00
        /// Nullable:False
        /// </summary>           
        public decimal GoodsPrice { get; private set; }

        /// <summary>
        /// Desc:商品划线价
        /// Default:0.00
        /// Nullable:False
        /// </summary>           
        public decimal LinePrice { get; private set; }

        /// <summary>
        /// Desc:商品重量(Kg)
        /// Default:0
        /// Nullable:False
        /// </summary>           
        public double GoodsWeight { get; private set; }

        /// <summary>
        /// Desc:购买数量
        /// Default:0
        /// Nullable:False
        /// </summary>           
        public int TotalNum { get; private set; }

        /// <summary>
        /// Desc:商品总价
        /// Default:0.00
        /// Nullable:False
        /// </summary>           
        public decimal TotalPrice { get; private set; }

        /// <summary>
        /// Desc:订单id
        /// Default:0
        /// Nullable:False
        /// </summary>           
        public int OrderId { get; private set; }

        /// <summary>
        /// Desc:用户id
        /// Default:0
        /// Nullable:False
        /// </summary>           
        public int AppUserId { get; private set; }


        public static OrderGoods Create(Goods goods, GoodsSpec goodsSpec, int goodsNum, int orderId, int appUserId)
        {
            OrderGoods orderGoods = new OrderGoods()
            {
                GoodsId = goods.Id,
                GoodsName = goods.Name,
                GoodsPrice = goodsSpec.GoodsPrice,
                LinePrice = goodsSpec.LinePrice,
                GoodsNo = goodsSpec.GoodsNo,
                Content = goods.Content,
                ImgUrl = goods.ImgUrl,
                AppUserId = appUserId,
                TotalNum = goodsNum,
                TotalPrice = goodsSpec.GoodsPrice * goodsNum,
                SpecType = goods.SpecType,
                GoodsAttr = goods.SpecMany,
                OrderId = orderId
            };
            return orderGoods;
        }

    }
}
