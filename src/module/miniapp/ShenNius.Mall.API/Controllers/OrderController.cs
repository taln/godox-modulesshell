﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Auth.API.Controllers;
using ShenNius.Auth.API.Infrastructure.Configs;
using ShenNius.Auth.API.Infrastructure.Enums.Extension;
using ShenNius.Auth.API.Models.Entity;
using ShenNius.Mall.API.Domain.Entity;
using ShenNius.Mall.API.Domain.Repository;
using ShenNius.Mall.API.Domain.ValueObjects;
using ShenNius.Mall.API.Dtos.Output;
using ShenNius.Repository;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ShenNius.Mall.API.Controllers
{
    /// <summary>
    /// 订单管理
    /// </summary>
    public class OrderController : AppBaseController
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderGoodsRepository _orderGoodsRepository;
        private readonly IBaseRepository<AppUserAddress> _appUserAddressRepository;
        private readonly IBaseRepository<AppUser> _appUserRepository;

        public OrderController(IOrderRepository orderRepository, IOrderGoodsRepository orderGoodsRepository, IBaseRepository<AppUserAddress> appUserAddressRepository, IBaseRepository<AppUser> appUserRepository)
        {
            _orderRepository = orderRepository;
            _orderGoodsRepository = orderGoodsRepository;
            _appUserAddressRepository = appUserAddressRepository;
            _appUserRepository = appUserRepository;
        }
        /// <summary>
        /// 取消订单
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="goodsId"></param>
        /// <returns></returns>
        [HttpPost]
        public Task<ApiResult> Cancel([FromForm] int orderId, [FromForm] int goodsId)
        {
            return _orderGoodsRepository.CancelOrderAsync(orderId, goodsId, HttpWx.TenantId);
        }
        /// <summary>
        /// 确认收货
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        [HttpPost]
        public Task<ApiResult> Receipt([FromForm] int orderId)
        {
            return _orderGoodsRepository.ReceiptAsync(orderId, HttpWx.AppUserId);

        }

        /// <summary>
        /// 小程序订单列表
        /// </summary>
        /// <param name="dataType">订单类型</param>
        /// <returns></returns>
        [HttpGet]
        public Task<ApiResult> GetList(string dataType)
        {
            return _orderGoodsRepository.GetListAsync(HttpWx.AppUserId, dataType);

        }
        [HttpGet]
        public Task<ApiResult<OrderDetailOutput>> Detail(int orderId)
        {
            return _orderRepository.GetOrderDetailAsync(orderId);
        }
        /// <summary>
        /// 立马就买
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public Task<ApiResult> BuyNow([FromForm] int goodsId, [FromForm] int goodsNum, [FromForm] string goodsSkuId)
        {
            return _orderGoodsRepository.BuyNowAsync(goodsId, goodsNum, goodsSkuId, HttpWx.AppUserId);
        }
        /// <summary>
        /// 购物车结算
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public Task<ApiResult> CartBuy()
        {
            return _orderGoodsRepository.CartBuyAsync(HttpWx.AppUserId);
        }

        /// <summary>
        /// 获取用户已经添加的收获地址
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetExistAddress()
        {
            //该收获地址 后面可能会做是否包邮判断           
            var addressModel = await _appUserAddressRepository.GetModelAsync(d => d.IsDeleted && d.AppUserId == HttpWx.AppUserId && d.IsDefault == true);
            if (addressModel == null)
            {
                addressModel = await _appUserAddressRepository.GetModelAsync(d => d.IsDeleted && d.AppUserId == HttpWx.AppUserId);
            }
            return new ApiResult(addressModel);
        }

        /// <summary>
        /// 用户中心订单统计
        /// </summary>
        /// <param name="wxappId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> Statistics()
        {
            var appUser = await _appUserRepository.GetModelAsync(d => d.IsDeleted && d.Id == HttpWx.AppUserId);
            var paymentCount = await GetOrderCount("payment");
            var receivedCount = await GetOrderCount("received");
            return new ApiResult(new { userInfo = appUser, orderCount = new { payment = paymentCount, received = receivedCount } });
        }

        private async Task<int> GetOrderCount(string type = "all")
        {
            Expression<Func<Order, bool>> where = null;
            switch (type)
            {
                case "payment":
                    where = l => l.PayStatus == PayStatusEnum.WaitForPay.GetValue<int>() && l.AppUserId == HttpWx.AppUserId;
                    break;
                case "received":
                    where = l => l.PayStatus == PayStatusEnum.Paid.GetValue<int>() && l.DeliveryStatus == DeliveryStatusEnum.Sended.GetValue<int>() && l.ReceiptStatus == ReceiptStatusEnum.WaitForReceiving.GetValue<int>() && l.AppUserId == HttpWx.AppUserId;
                    break;
                case "all":
                    break;
            }
            var result = await _orderRepository.CountAsync(where);
            return result;
        }
    }
}
