﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Auth.API.Controllers;
using ShenNius.Auth.API.Infrastructure.Configs;
using ShenNius.Auth.API.Infrastructure.Enums.Extension;
using ShenNius.Mall.API.Domain.Entity;
using ShenNius.Mall.API.Domain.Repository;
using ShenNius.Mall.API.Domain.ValueObjects;
using ShenNius.Mall.API.Dtos.Output;
using ShenNius.Repository;
using SqlSugar;
using System.Linq;
using System.Threading.Tasks;

namespace ShenNius.Mall.API.Controllers
{/// <summary>
/// 购物车
/// </summary>
    public class CartController : AppBaseController
    {
        private readonly IBaseRepository<Cart> _cartRepository;
        private readonly ISqlSugarClient _db;
        private readonly IGoodsRepository _goodRepository;

        public CartController(IBaseRepository<Cart> cartRepository, ISqlSugarClient db, IGoodsRepository goodRepository)
        {
            _cartRepository = cartRepository;
            _db = db;
            _goodRepository = goodRepository;
        }
        /// <summary>
        /// 购物车删除
        /// </summary>
        /// <param name="goodsId"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> Delete([FromForm] int goodsId)
        {
            var deleteModel = new Cart();
            deleteModel.SoftDelete();
            var i = await _cartRepository.UpdateAsync(d => deleteModel, d => d.GoodsId == goodsId && d.AppUserId == HttpWx.AppUserId);
            return Result(i);
        }
        [HttpPost]
        public async Task<ApiResult> Add([FromForm] int goodsId, [FromForm] int goodsNum, [FromForm] string specSkuId)
        {
            var goodsData = await _goodRepository.GoodInfoIsExist(goodsId, goodsNum, specSkuId, HttpWx.AppUserId);
            var isExistCartModel = await _cartRepository.GetModelAsync(l => l.AppUserId == HttpWx.AppUserId && l.GoodsId == goodsId);
            int i = 0;
            if (isExistCartModel?.Id > 0)
            {
                goodsNum += isExistCartModel.GoodsNum;
                isExistCartModel.AddGoodsNum(goodsNum, specSkuId);
                i = await _cartRepository.UpdateAsync(isExistCartModel);
            }
            else
            {
                Cart model = Cart.Create(goodsData.Item1.Id, HttpWx.AppUserId, goodsNum, specSkuId);
                i = await _cartRepository.AddAsync(model);
            }
            return Result(i);
        }
        /// <summary>
        /// 减掉商品数量
        /// </summary>
        /// <param name="goodsId"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> Sub([FromForm] int goodsId)
        {
            var isExistCartModel = await _cartRepository.GetModelAsync(l => l.AppUserId == HttpWx.AppUserId && l.GoodsId == goodsId);

            if (isExistCartModel?.Id != null)
            {
                if (isExistCartModel.GoodsNum < 1)
                {
                    return new ApiResult("该商品在购物车已经不存在了");
                }
                isExistCartModel.SubGoodsNum(1);
                await _cartRepository.UpdateAsync(isExistCartModel);
            }
            return new ApiResult(msg: "删减成功", 200);
        }
        [HttpGet]
        public async Task<ApiResult> Lists(int tenantId)
        {
            var cartList = await _cartRepository.GetListAsync(l => l.AppUserId == HttpWx.AppUserId);
            var inCludeGoods = cartList.Select(d => d.GoodsId).ToList();
            var goodsList = await _db.Queryable<Goods, GoodsSpec>((g, gc) => new JoinQueryInfos(JoinType.Inner, g.Id == gc.GoodsId))
               .Where((g, gc) => g.TenantId == tenantId)
               .Select((g, gc) => new CartGoodsOutput
               {
                   GoodsId = g.Id,
                   ImgUrl = g.ImgUrl,
                   GoodsPrice = gc.GoodsPrice,
                   SpecType = g.SpecType,
                   LinePrice = gc.LinePrice,
                   GoodsSales = gc.GoodsSales,
                   SalesActual = g.SalesActual,
                   SpecMany = g.SpecMany
               }).ToListAsync();

            double totalPrice = 0;
            foreach (var item in goodsList)
            {
                //去查商品对应的购物车中的数量
                var cartGoodsNum = cartList.Where(d => d.GoodsId == item.GoodsId).Select(d => d.GoodsNum).FirstOrDefault();
                item.OrderTotalNum = cartGoodsNum;
                totalPrice += (double)item.GoodsPrice * cartGoodsNum;
                if (item.SpecType == SpecTypeEnum.Multi.GetValue<int>())
                {
                    //显示规格组和值

                }
            }
            return new ApiResult(new
            {
                GoodsList = goodsList, //单价*数量 
                OrderTotalPrice = totalPrice
            });
        }

    }
}
