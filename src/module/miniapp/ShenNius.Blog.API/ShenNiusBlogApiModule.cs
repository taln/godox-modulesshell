﻿using Microsoft.Extensions.DependencyInjection;
using ShenNius.Auth.API;
using ShenNius.ModuleCore;
using ShenNius.ModuleCore.Context;

namespace ShenNius.Blog.API
{
    [DependsOn(typeof(ShenNiusAuthApiModule))]
    public class ShenNiusBlogApiModule : AppModule
    {
        public override void OnConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddAutoMapper(typeof(AutomapperProfile));
            context.Services.AddHttpClient();
        }
        public override void OnApplicationInitialization(ApplicationInitializationContext context)
        {
        }
    }
}
