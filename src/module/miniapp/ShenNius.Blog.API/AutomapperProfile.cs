﻿using AutoMapper;
using ShenNius.Blog.API.Domain.Entity;
using ShenNius.Blog.API.Dtos.Input;

namespace ShenNius.Blog.API
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<MessageInput, Message>();
        }
    }
}
