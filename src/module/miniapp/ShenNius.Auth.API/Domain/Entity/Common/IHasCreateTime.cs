﻿using System;

namespace ShenNius.Auth.API.Domain.Entity.Common
{
    public interface IHasCreateTime
    {
        DateTime CreateTime { get; }
    }
}
