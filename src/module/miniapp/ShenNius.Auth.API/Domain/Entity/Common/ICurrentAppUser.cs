﻿namespace ShenNius.Auth.API.Domain.Entity.Common
{
    public interface ICurrentAppUser
    {
        public int AppUserId { get; set; }
        public int TenantId { get; set; }
    }
}
