﻿namespace ShenNius.Cms.API.Dtos.Output
{
    public class KeywordOutput
    {
        public string Title { get; set; }
        public string Url { get; set; }
    }
}
