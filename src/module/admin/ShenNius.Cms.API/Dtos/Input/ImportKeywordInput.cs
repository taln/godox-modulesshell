﻿using Magicodes.ExporterAndImporter.Core;
using Magicodes.ExporterAndImporter.Excel;
using ShenNius.Sys.API.Domain.Entity.Common;
using System.ComponentModel.DataAnnotations;

namespace ShenNius.Cms.API.Dtos.Input
{
    /// <summary>
    /// 导入关键词实体DTO
    /// </summary>
    [ExcelImporter(IsLabelingError = true)]
    public class ImportKeywordInput : IGlobalTenant
    {
        [ImporterHeader(Name = "租户Id")]
        [Required(ErrorMessage = "租户Id")]
        public int TenantId { get; set; }
        [ImporterHeader(Name = "关键词名称")]
        [Required(ErrorMessage = "关键词名称不能为空")]
        public string Title { get; set; }
        [ImporterHeader(Name = "关键词链接")]
        [Required(ErrorMessage = "关键词链接不能为空")]
        public string Url { get; set; }

    }
}
