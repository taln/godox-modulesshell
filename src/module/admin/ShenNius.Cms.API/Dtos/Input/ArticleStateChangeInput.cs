﻿using ShenNius.Sys.API.Dtos.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShenNius.Cms.API.Dtos.Input
{
    public class ArticleStateChangeInput:GlobalTenantInput
    {
        public int Id { get; set; }
        public string  Type { get; set; }
        public bool  StateValue { get; set; }
    }
}
