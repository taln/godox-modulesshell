﻿using ShenNius.Sys.API.Domain.Entity.Common;
using ShenNius.Sys.API.Dtos.Common;
using System;

/*************************************
* 类名：KeywordModifyInput
* 作者：realyrare
* 邮箱：mahonggang8888@126.com
* 时间：2021/3/31 19:23:47
*┌───────────────────────────────────┐　    
*│　     版权所有：神牛软件　　　　	 │
*└───────────────────────────────────┘
**************************************/

namespace ShenNius.Cms.API.Dtos.Input
{
    public class KeywordModifyInput : GlobalTenantInput, IEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public DateTime ModifyTime { get; set; } = DateTime.Now;
    }
}