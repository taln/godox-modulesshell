﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Cms.API.Domain.Entity;
using ShenNius.Repository;
using ShenNius.Sys.API.Controllers;
using ShenNius.Sys.API.Domain.Repository;
using ShenNius.Sys.API.Domain.ValueObjects.Enum;
using ShenNius.Sys.API.Dtos.Common;
using ShenNius.Sys.API.Infrastructure.Attributes;
using ShenNius.Sys.API.Infrastructure.Configs;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

/*************************************
* 类名：MessageController
* 作者：realyrare
* 邮箱：mahonggang8888@126.com
* 时间：2021/4/16 14:40:43
*┌───────────────────────────────────┐　    
*│　   版权所有：神牛软件　　　　	     │
*└───────────────────────────────────┘
**************************************/

namespace ShenNius.Cms.API.Controllers
{
    [Route("api/cms/[controller]/[action]")]
    [MultiTenant]
    public class MessageController : ApiControllerBase
    {
        private readonly IBaseRepository<Message> _repository;
        private readonly IRecycleRepository _recycleRepository;

        public MessageController(IBaseRepository<Message> repository, IRecycleRepository recycleRepository)
        {
            _repository = repository;
            _recycleRepository = recycleRepository;
        }
        [HttpGet, Authority]
        public async Task<ApiResult> GetListPages([FromQuery] KeyListTenantQuery query)
        {
            Expression<Func<Message, bool>> whereExpression = d => d.IsDeleted == false;
            if (query.TenantId > 0)
            {
                whereExpression = d => d.TenantId == query.TenantId;
            }
            if (!string.IsNullOrEmpty(query.Key))
            {
                whereExpression = d => d.UserName.Contains(query.Key) && !d.IsDeleted;
            }
            var res = await _repository.GetPagesAsync(query.Page, query.Limit, whereExpression, d => d.Id, false);
            return new ApiResult(data: new { count = res.TotalItems, items = res.Items });
        }
        /// <summary>
        /// 批量真实删除
        /// </summary>
        /// <param name="deleteInput"></param>
        /// <returns></returns>
        [HttpDelete, Authority]
        public async Task<ApiResult> Deletes([FromBody] DeletesTenantInput deleteInput)
        {
            var res = await _repository.DeleteAsync(deleteInput.Ids);
            if (res <= 0)
            {
                return new ApiResult("删除失败了！");
            }
            return new ApiResult();
        }
        [HttpDelete, Authority(Action = nameof(BtnEnum.Delete))]
        public virtual Task<ApiResult> SoftDelete([FromBody] DeletesTenantInput input)
        {
            return _recycleRepository.SoftDeleteAsync(input, _repository);
        }
    }
}