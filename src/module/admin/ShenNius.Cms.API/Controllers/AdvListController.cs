﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ShenNius.Cms.API.Domain.Entity;
using ShenNius.Cms.API.Dtos.Input;
using ShenNius.Cms.API.Dtos.Output;
using ShenNius.Repository;
using ShenNius.Repository.Extensions;
using ShenNius.Sys.API.Controllers;
using ShenNius.Sys.API.Domain.Entity;
using ShenNius.Sys.API.Dtos.Common;
using ShenNius.Sys.API.Infrastructure.Configs;
using SqlSugar;
using System.Threading.Tasks;

/*************************************
* 类 名： AdvListController
* 作 者： realyrare
* 邮 箱： mahonggang8888@126.com
* 时 间： 2021/3/16 18:09:15
* .netV： 3.1
*┌───────────────────────────────────┐　    
*│　     版权所有：神牛软件　　　　	 │
*└───────────────────────────────────┘
**************************************/

namespace ShenNius.Cms.API.Controllers
{
    [Route("api/cms/[controller]/[action]")]
    public class AdvListController : ApiTenantBaseController<AdvList, DetailTenantQuery, DeletesTenantInput, KeyListTenantQuery, AdvListInput, AdvListModifyInput>
    {
        private readonly ISqlSugarClient _db;
        public AdvListController(IBaseRepository<AdvList> repository, IMapper mapper, ISqlSugarClient db) : base(repository, mapper)
        {
            _db = db;
        }
        [HttpGet]
        public override async Task<ApiResult> GetListPages([FromQuery] KeyListTenantQuery query)
        {
            var res = await _db.Queryable<AdvList>().Where(d => !d.IsDeleted && d.TenantId == query.TenantId)
               .WhereIF(!string.IsNullOrEmpty(query.Key), d => d.Title.Contains(query.Key))
                  .OrderBy(c => c.CreateTime, OrderByType.Desc)
               .Select(d => new AdvListOutput()
               {
                   TenantName = SqlFunc.Subqueryable<Tenant>().Where(s => s.Id == d.TenantId).Select(s => s.Name),
                   Id = d.Id,
                   CreateTime = d.CreateTime,
                   Type = d.Type,
                   ModifyTime = d.ModifyTime,
                   Summary = d.Summary,
                   Title = d.Title,
               }
               ).ToPageAsync(query.Page, query.Limit);
            return new ApiResult(data: new { count = res.TotalItems, items = res.Items });
        }

    }
}