﻿using ShenNius.Repository;
using ShenNius.Shop.API.Domain.Entity;
using ShenNius.Shop.API.Dtos.Input;
using ShenNius.Sys.API.Dtos.Common;
using ShenNius.Sys.API.Infrastructure.Configs;
using SqlSugar;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ShenNius.Shop.API.Domain.Repository
{
    public interface IGoodsRepository : IBaseRepository<Goods>
    {
        Task<ApiResult> AddAsync(GoodsInput input);
        Task<ApiResult> ModifyAsync(GoodsModifyInput input);
        Task<ApiResult<GoodsModifyInput>> DetailAsync(int id);
        /// <summary>
        /// 添加规格组名称和值
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ApiResult> AddSpecAsync(SpecInput input);
        /// <summary>
        /// 添加规格组值
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ApiResult> AddSpecAsync(SpecValuesInput input);

        Task<ApiResult> GetListPageAsync(KeyListTenantQuery query);
        /// <summary>
        /// 小程序首页
        /// </summary>
        /// <returns></returns>
        Task<ApiResult> GetByWherePageAsync(ListTenantQuery query, Expression<Func<Goods, Category, GoodsSpec, object>> orderBywhere, OrderByType sort, Expression<Func<Goods, Category, GoodsSpec, bool>> where = null);
        /// <summary>
        /// 立即购买
        /// </summary>
        /// <returns></returns>
        Task<ApiResult> GetBuyNowAsync(int goodsId, int goodsNum, string goodsNo, int tenantId);
        /// <summary>
        /// 商品信息判断（添加购物车，下单共用）
        /// </summary>
        /// <param name="goodsId"></param>
        /// <param name="goodsNum"></param>
        /// <param name="specSkuId"></param>
        /// <param name="appUserId"></param>
        /// <returns></returns>
        Task<Tuple<Goods, GoodsSpec>> GoodInfoIsExist(int goodsId, int goodsNum, string specSkuId, int appUserId);
    }
}
