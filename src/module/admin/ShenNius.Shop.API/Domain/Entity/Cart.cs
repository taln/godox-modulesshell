﻿using ShenNius.Sys.API.Domain.Entity.Common;
using SqlSugar;

namespace ShenNius.Shop.API.Domain.Entity
{
    [SugarTable("shop_cart")]
    public class Cart : BaseTenantEntity, IAggregateRoot
    {
        public int GoodsNum { get; private set; }
        public int AppUserId { get; private set; }
        public int GoodsId { get; private set; }
        public string SpecSkuId { get; private set; }
    }
}
