﻿using ShenNius.Sys.API.Dtos.Common;

namespace ShenNius.Shop.API.Dtos.Input
{
    public class CategoryInput : GlobalTenantInput
    {
        public int ParentId { get; set; }
        public string IconSrc { get; set; }
        public string Name { get; set; }
    }
}
