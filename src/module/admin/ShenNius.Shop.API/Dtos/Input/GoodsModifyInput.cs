﻿using System;

namespace ShenNius.Shop.API.Dtos.Input
{
    public class GoodsModifyInput : GoodsInput
    {
        public int Id { get; set; }

        public DateTime ModifyTime { get; set; } = DateTime.Now;

    }
}
