﻿using FluentValidation;
using ShenNius.Shop.API.Dtos.Input;

/*************************************
* 类名：CategoryInputValidator
* 作者：realyrare
* 邮箱：mahonggang8888@126.com
* 时间：2021/8/10 9:46:45
*┌───────────────────────────────────┐　    
*│　   版权所有：神牛软件　　　　	     │
*└───────────────────────────────────┘
**************************************/

namespace ShenNius.Shop.API.Dtos.Validators
{
    public class CategoryInputValidator : AbstractValidator<CategoryInput>
    {
        public CategoryInputValidator()
        {
            CascadeMode = CascadeMode.Stop;
            RuleFor(x => x.Name).NotEmpty().WithMessage("标题必须填写");

        }
    }
}