﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ShenNius.Wechat.API.Dtos
{


    #region 素材模型组
    /// <summary>
    /// 素材组
    /// </summary>
    public class MeterList
    {
        /// <summary>
        /// 该类型的素材的总数
        /// </summary>
        [JsonProperty("total_count")]
        public int TotalCount { get; set; }
        /// <summary>
        /// 本次调用获取的素材的数量
        /// </summary>
        [JsonProperty("item_count")]
        public int ItemCount { get; set; }
        [JsonProperty("item")]
        public List<MeterItem> Item { get; set; }
    }
    #endregion
    /// <summary>
    /// 素材集合
    /// </summary>
    public class MeterItem
    {
        /// <summary>
        /// 素材id
        /// </summary>
        [JsonProperty("media_id")]
        public string MediaId { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        [JsonProperty("update_time")]
        public int UpdateTime { get; set; }
        [JsonProperty("content")]
        public MeterContent Content { get; set; }
    }
    public class MeterContent
    {
        /// <summary>
        /// 创建时间
        /// </summary>
        [JsonProperty("create_time")]
        public int CreateTime { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [JsonProperty("update_time")]
        public int UpdateTime { get; set; }
        [JsonProperty("news_item")]
        public List<MeterNewsItem> NewsItem { get; set; }
    }
    public class MeterNewsItem
    {
        /// <summary>
        /// 图文消息的标题
        /// </summary>
        [JsonProperty("Title")]
        public string title { get; set; }
        /// <summary>
        /// 文件名称
        /// </summary>
        //public string name { get; set; }

        /// <summary>
        /// 作者
        /// </summary>
        [JsonProperty("author")]
        public string Author { get; set; }
        /// <summary>
        /// 图文消息的摘要，仅有单图文消息才有摘要，多图文此处为空
        /// </summary>
        [JsonProperty("digest")]
        public string Digest { get; set; }
        /// <summary>
        /// 图文消息的具体内容，支持HTML标签，必须少于2万字符，小于1M，且此处会去除JS
        /// </summary>
        [JsonProperty("content")]
        public string Content { get; set; }
        /// <summary>
        /// 图文消息的原文地址，即点击“阅读原文”后的URL
        /// </summary>
        [JsonProperty("content_source_url")]
        public string ContentSourceUrl { get; set; }
        /// <summary>
        /// 图文消息的封面图片素材id（必须是永久mediaID）
        /// </summary>
        [JsonProperty("thumb_media_id")]
        public string ThumbMediaId { get; set; }
        /// <summary>
        /// 图文页的URL，或者，当获取的列表是图片素材列表时，该字段是图片的URL
        /// </summary>
        [JsonProperty("url")]
        public string Url { get; set; }
        [JsonProperty("thumb_url")]
        public string ThumbUrl { get; set; }
        /// <summary>
        /// 是否显示封面，0为false，即不显示，1为true，即显示
        /// </summary>
        [JsonProperty("show_cover_pic")]
        public int ShowCoverPic { get; set; }
        [JsonProperty("need_open_comment")]
        public int NeedOpenComment { get; set; }
        [JsonProperty("only_fans_can_comment")]
        public int OnlyFansCanComment { get; set; }
    }
}

