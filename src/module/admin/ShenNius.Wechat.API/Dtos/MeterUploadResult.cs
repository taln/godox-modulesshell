﻿using Newtonsoft.Json;

namespace ShenNius.Wechat.API.Dtos
{
    public class MeterUploadResult
    {
        /// <summary>
        /// 永久素材的id
        /// </summary>
        [JsonProperty("media_id")]
        public string MediaId { get; set; }
        /// <summary>
        /// 素材的地址
        /// </summary>
        [JsonProperty("url")]
        public string Url { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [JsonProperty("code")]
        public int Code { get; set; } = 200;
    }
}
