﻿using System;

namespace ShenNius.Sys.API.Domain.Entity.Common
{
    public interface IHasModifyTime
    {
        DateTime? ModifyTime { get; }
    }
}
