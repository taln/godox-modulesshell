﻿using System;

namespace ShenNius.Sys.API.Domain.Entity.Common
{
    public interface IHasCreateTime
    {
        DateTime CreateTime { get; }
    }
}
