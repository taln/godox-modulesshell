﻿namespace ShenNius.Sys.API.Domain.Entity.Common
{
    public interface IEntity
    {
        int Id { get; }
    }
}
