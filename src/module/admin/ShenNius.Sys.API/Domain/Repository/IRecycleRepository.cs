﻿using ShenNius.Repository;
using ShenNius.Sys.API.Domain.Entity;
using ShenNius.Sys.API.Domain.Entity.Common;
using ShenNius.Sys.API.Dtos.Common;
using ShenNius.Sys.API.Infrastructure.Configs;
using System.Threading.Tasks;

namespace ShenNius.Sys.API.Domain.Repository
{
    public interface IRecycleRepository : IBaseRepository<Recycle>
    {
        Task<ApiResult> RestoreAsync(DeletesInput deletesInput);
        Task<ApiResult> RealyDeleteAsync(DeletesInput deletesInput);
        Task<ApiResult> SoftDeleteAsync<TEntity>(DeletesTenantInput input, IBaseRepository<TEntity> service) where TEntity : BaseTenantEntity, new();
        Task<ApiResult> SoftDeleteAsync<TEntity>(DeletesInput input, IBaseRepository<TEntity> service) where TEntity : BaseEntity, new();
        Task<ApiResult> GetPagesAsync(KeyListTenantQuery query);
    }
}
