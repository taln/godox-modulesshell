﻿using FluentValidation;
using ShenNius.Sys.API.Dtos.Common;

namespace ShenNius.Sys.API.Dtos.Validators
{
    public class CommonDeleteInputValidator : AbstractValidator<DeletesInput>
    {
        public CommonDeleteInputValidator()
        {
            RuleFor(x => x.Ids.Count).NotEmpty().WithMessage("Id必须传递");
        }
    }
}
