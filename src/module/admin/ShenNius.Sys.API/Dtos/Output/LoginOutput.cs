﻿using System;
using System.Collections.Generic;

namespace ShenNius.Sys.API.Dtos.Output
{
    public class LoginOutput
    {
        public int Id { get; set; }
        public string LoginName { get; set; }
        public string Mobile { get; set; }
        public DateTime LastLoginTime { get; set; }
        public string TrueName { get; set; }
        public string Token { get; set; }
        /// <summary>
        /// 权限
        /// </summary>
        public List<MenuAuthOutput> MenuAuthOutputs { get; set; } = new List<MenuAuthOutput>();
    }
}
