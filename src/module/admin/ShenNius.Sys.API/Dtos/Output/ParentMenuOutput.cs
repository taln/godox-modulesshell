﻿namespace ShenNius.Sys.API.Dtos.Output
{
    public class ParentMenuOutput
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
