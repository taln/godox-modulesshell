﻿using System;

namespace ShenNius.Sys.API.Dtos.Input
{
    public class ModifyPwdInput
    {
        public int Id { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
        /// <summary>
        /// 旧密码
        /// </summary>
        public string OldPassword { get; set; }
        public void ModifyPassword(int userId)
        {
            if (Id == 0)
            {
                Id = userId;
            }
            if (!ConfirmPassword.Equals(NewPassword))
            {
                throw new ArgumentException("两次输入的密码不一致");
            }
        }
    }
}
