﻿using System;

namespace ShenNius.Sys.API.Dtos.Input
{
    public class RoleInput
    {
        public string Name { get; set; }

        public string Description { get; set; }
        public DateTime CreateTime { get; set; } = DateTime.Now;
    }
}
