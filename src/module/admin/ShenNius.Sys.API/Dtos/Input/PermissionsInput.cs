﻿namespace ShenNius.Sys.API.Dtos.Input
{
    public class PermissionsInput
    {
        public int RoleId { get; set; }

        /// <summary>
        /// Desc:
        /// Default:
        /// Nullable:False
        /// </summary>           
        public int MenuId { get; set; }
    }
}
