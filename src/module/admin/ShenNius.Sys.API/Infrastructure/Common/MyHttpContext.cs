﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace ShenNius.Sys.API.Infrastructure.Common
{
    public class MyHttpContext
    {
        public static IServiceProvider ServiceProvider;
        public static HttpContext Current
        {
            get
            {
                var httpContextAccessor = ServiceProvider.GetService<IHttpContextAccessor>();
                if (httpContextAccessor == null)
                {
                    throw new ArgumentNullException(nameof(httpContextAccessor));
                }
                return httpContextAccessor?.HttpContext;
            }

        }
    }
}
