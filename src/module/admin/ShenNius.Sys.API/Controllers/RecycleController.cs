﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Sys.API.Domain.Repository;
using ShenNius.Sys.API.Dtos.Common;
using ShenNius.Sys.API.Infrastructure.Attributes;
using ShenNius.Sys.API.Infrastructure.Configs;
using System.Threading.Tasks;

/*************************************
* 类名：RecycleController
* 作者：realyrare
* 邮箱：mahonggang8888@126.com
* 时间：2021/4/8 19:24:46
*┌───────────────────────────────────┐　    
*│　   版权所有：神牛软件　　　　	     │
*└───────────────────────────────────┘
**************************************/

namespace ShenNius.Sys.API.Controllers
{
    /// <summary>
    /// 回收站
    /// </summary>
    public class RecycleController : ApiControllerBase
    {
        private readonly IRecycleRepository _recycleRepository;
        public RecycleController(IRecycleRepository recycleRepository)
        {
            _recycleRepository = recycleRepository;
        }
        /// <summary>
        /// 彻底删除
        /// </summary>
        /// <param name="commonDeleteInput"></param>
        /// <returns></returns>
        [HttpDelete, Authority]

        public Task<ApiResult> Deletes([FromBody] DeletesInput commonDeleteInput)
        {
            return _recycleRepository.RealyDeleteAsync(commonDeleteInput);
        }

        [HttpGet, Authority]
        [MultiTenant]
        public Task<ApiResult> GetListPages([FromQuery] KeyListTenantQuery query)
        {
            return _recycleRepository.GetPagesAsync(query);
        }

        /// <summary>
        /// 数据还原
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost, Authority]
        public Task<ApiResult> Restore([FromBody] DeletesInput input)
        {
            return _recycleRepository.RestoreAsync(input);
        }
    }
}