﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Repository;
using ShenNius.Sys.API.Domain.Entity;
using ShenNius.Sys.API.Dtos.Common;
using ShenNius.Sys.API.Infrastructure.Attributes;
using ShenNius.Sys.API.Infrastructure.Configs;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ShenNius.Sys.API.Controllers
{
    public class LogsController : ApiControllerBase
    {
        private readonly IBaseRepository<Log> _logRepository;
        public LogsController(IBaseRepository<Log> logRepository)
        {
            _logRepository = logRepository;
        }


        [HttpDelete, Authority]
        public async Task<ApiResult> Deletes([FromBody] DeletesInput commonDeleteInput)
        {
            return new ApiResult(await _logRepository.DeleteAsync(commonDeleteInput.Ids));
        }

        [HttpGet, Authority]
        public async Task<ApiResult> GetListPages(int page, int limit = 15, string key = null)
        {
            Expression<Func<Log, bool>> whereExpression = null;
            if (!string.IsNullOrEmpty(key))
            {
                whereExpression = d => d.Message.Contains(key);
            }
            var res = await _logRepository.GetPagesAsync(page, limit, whereExpression, d => d.Id, false);
            return new ApiResult(data: new { count = res.TotalItems, items = res.Items });
        }

        [HttpGet, Authority]
        public async Task<ApiResult> Detail(int id)
        {
            var res = await _logRepository.GetModelAsync(d => d.Id == id);
            return new ApiResult(data: res);
        }
    }
}
