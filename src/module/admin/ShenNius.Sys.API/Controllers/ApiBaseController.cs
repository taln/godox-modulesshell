﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ShenNius.Repository;
using ShenNius.Sys.API.Domain.Entity.Common;
using ShenNius.Sys.API.Dtos.Common;
using ShenNius.Sys.API.Infrastructure.Attributes;
using ShenNius.Sys.API.Infrastructure.Configs;
using System.Threading.Tasks;

/*************************************
* 类名：ApiBaseController
* 作者：realyrare
* 邮箱：mahonggang8888@126.com
* 时间：2021/4/1 10:28:20
*┌───────────────────────────────────┐　    
*│　     版权所有：神牛软件　　　　	 │
*└───────────────────────────────────┘
**************************************/

namespace ShenNius.Sys.API.Controllers
{
    /// <summary>
    /// 适用于非多租户的模块使用
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TDetailQuery"></typeparam>
    /// <typeparam name="TDeleteInput"></typeparam>
    /// <typeparam name="TListQuery"></typeparam>
    /// <typeparam name="TCreateInput"></typeparam>
    /// <typeparam name="TUpdateInput"></typeparam>
    [ApiController]
    public abstract class ApiBaseController<TEntity, TDetailQuery, TDeleteInput, TListQuery, TCreateInput, TUpdateInput> : ControllerBase
       where TEntity : BaseEntity, new()
       where TDeleteInput : DeletesInput
       where TDetailQuery : DetailQuery
       where TListQuery : PageQuery
       where TUpdateInput : IEntity
    {
        private readonly IBaseRepository<TEntity> _repository;
        private readonly IMapper _mapper;
        public ApiBaseController(IBaseRepository<TEntity> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        /// <summary>
        /// 真实批量删除
        /// </summary>
        /// <param name="commonDeleteInput"></param>
        /// <returns></returns>
        [HttpDelete, Authority]
        public virtual async Task<ApiResult> Deletes([FromBody] TDeleteInput commonDeleteInput)
        {
            var res = await _repository.DeleteAsync(commonDeleteInput.Ids);
            return res <= 0 ? new ApiResult("删除失败了！") : new ApiResult();
        }
        [HttpGet, Authority]
        public virtual async Task<ApiResult> GetListPages([FromQuery] TListQuery listQuery)
        {
            var res = await _repository.GetPagesAsync(listQuery.Page, listQuery.Limit, d => d.IsDeleted == false, d => d.Id, false);
            return new ApiResult(data: new { count = res.TotalItems, items = res.Items });
        }

        [HttpGet, Authority]
        public virtual async Task<ApiResult> Detail([FromQuery] TDetailQuery detailQuery)
        {
            var res = await _repository.GetModelAsync(d => d.Id == detailQuery.Id && d.IsDeleted == false);
            return new ApiResult(data: res);
        }
        [HttpPost, Authority]
        public virtual async Task<ApiResult> Add([FromBody] TCreateInput createInput)
        {
            var entity = _mapper.Map<TEntity>(createInput);
            var res = await _repository.AddAsync(entity);
            return res <= 0 ? new ApiResult("添加失败了！") : new ApiResult();
        }
        [HttpPut, Authority]
        public virtual async Task<ApiResult> Modify([FromBody] TUpdateInput updateInput)
        {
            var entity = _mapper.Map<TEntity>(updateInput);
            var model = await _repository.GetModelAsync(d => d.IsDeleted == false && d.Id == updateInput.Id);
            if (model == null || (model?.Id) <= 0)
            {
                return new ApiResult($"该{updateInput.Id}为查询到对应的数据！");
            }
            var res = await _repository.UpdateAsync(entity, d => new { d.CreateTime });
            return res <= 0 ? new ApiResult("修改失败了！") : new ApiResult();
        }
    }
}