using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using NLog.Web;
using ShenNius.ModuleCore.Extensions;
using ShenNius.Mvc.Admin;


var builder = WebApplication.CreateBuilder(args);
var mvcBuilder = builder.Services.AddControllersWithViews();
#if DEBUG
mvcBuilder.AddRazorRuntimeCompilation();
#endif
builder.Logging.AddNLog("nlog.config");
builder.Services.AddModule<ShenNiusMvcAdminModule>(builder.Configuration);
var app = builder.Build();
app.UseModule();
app.Run();