﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Cms.API.Domain.Entity;
using ShenNius.Repository;
using System.Threading.Tasks;

namespace ShenNius.Mvc.Admin.Areas.Cms.Controllers
{
    [Area("cms")]
    public class ColumnController : Controller
    {
        private readonly IBaseRepository<Column> _columnService;

        public ColumnController(IBaseRepository<Column> columnService)
        {
            _columnService = columnService;
        }
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> Modify(int id = 0)
        {
            Column model = id == 0 ? new Column() : await _columnService.GetModelAsync(d => d.Id == id && !d.IsDeleted);
            return View(model);
        }
    }
}
