﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Repository;
using ShenNius.Sys.API.Domain.Entity;
using ShenNius.Sys.API.Domain.ValueObjects;
using System.Threading.Tasks;

namespace ShenNius.Mvc.Admin.Areas.Sys.Controllers
{
    [Area("sys")]
    public class ConfigController : Controller
    {
        private readonly IBaseRepository<Config> _configRepository;

        public ConfigController(IBaseRepository<Config> configRepository)
        {
            _configRepository = configRepository;
        }
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> Modify(int id)
        {
            var model = id == 0 ? new Config() : await _configRepository.GetModelAsync(d => d.Id == id && d.IsDeleted == false);
            return View(model);
        }
        [HttpGet]
        public async Task<IActionResult> Setting()
        {
            var value = await SysSetting.ReadAsync();
            return View(value);
        }
    }
}
