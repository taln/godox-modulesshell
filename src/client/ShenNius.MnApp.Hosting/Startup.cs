using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ShenNius.ModuleCore.Extensions;

namespace ShenNius.MnApp.Hosting
{
    public class Startup
    {

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddModule<ShenNiusMnAppHostModule>(Configuration);
        }
        public void Configure(IApplicationBuilder app)
        {
            app.UseModule();
        }
    }
}
